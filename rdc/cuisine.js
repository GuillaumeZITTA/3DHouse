import * as THREE from '../node_modules/three/build/three.module.js';
import * as Consts from '../common/consts.js';
import { materials, realSizeMaterial } from '../common/materials.js';
import { deg2rad } from '../common/utils.js';
import { newMesh, newLightGlobe, boxMur } from '../common/objects.js';
import { mur, porte } from '../common/murs.js';

"use strict";

function Cuisine(scene) {
    var spaceCuisine = new THREE.Object3D();
    scene.add(spaceCuisine);
    spaceCuisine.rotation.z = deg2rad(50);

    var cheneMaterialGenerator = new realSizeMaterial(0.4, 0.4, 'materiaux/chene.png');

    /**
     * Light definition
     */
    var ampoule = newLightGlobe(spaceCuisine);
    ampoule.position.set(0, 3.28/2, 2.4);

    /**
     * Mur evier
     */
    var mur_evier = new mur(spaceCuisine, 2.35, 3.28 / 2, 3.28, -90,
        materials.white);

    // Colonne chaudière
    (new boxMur(mur_evier, 0.6, Consts.hauteur_colonnes, 0.6, materials.white))
        .setPosition(0, Consts.hauteur_pied)
        .setFaceMaterial(materials.red)
        .setRightMaterial(cheneMaterialGenerator.getForSurface(0.6, Consts.hauteur_colonnes));

    // Lave vaisselle
    (new boxMur(mur_evier, 0.6, 0.6, 0.6, materials.white))
        .setPosition(0.6, Consts.hauteur_pied)
        .setFaceMaterial(materials.white);

    // Sous évier
    (new boxMur(mur_evier, 0.8, 0.6, 0.6, materials.white))
        .setPosition(1.2, Consts.hauteur_pied)
        .setFaceMaterial(materials.blue);

    // Tiroir
    (new boxMur(mur_evier, 0.6, 0.6, 0.6, materials.white))
        .setPosition(2.0, Consts.hauteur_pied)
        .setFaceMaterial(materials.red);

    // Rattrapage
    (new boxMur(mur_evier, 0.08, 0.6, 0.6, materials.white))
        .setPosition(2.6, Consts.hauteur_pied)
        .setFaceMaterial(materials.white);

    // Angle
    (new boxMur(mur_evier, 0.60, 0.6, 0.94, materials.white))
        .setPosition(2.68, Consts.hauteur_pied)
        .setFaceMaterial(materials.white);

    // Bandeau 
    (new boxMur(mur_evier, 3.28, Consts.hauteur_pied, 0.55, materials.white))
        .setPosition(0, 0)
        .setFaceMaterial(materials.white);

    // Meuble haut avec microonde
    (new boxMur(mur_evier, 0.6, 0.7, 0.35, materials.white))
        .setPosition(0.6, Consts.base_meuble_haut)
        .setFaceMaterial(materials.white);

    // Meuble haut sur évier
    (new boxMur(mur_evier, 0.8, 0.7, 0.35, materials.white))
        .setPosition(0.6 + 0.6, Consts.base_meuble_haut)
        .setFaceMaterial(materials.blue);

    // Meuble haut
    (new boxMur(mur_evier, 0.6, 0.7, 0.35, materials.white))
        .setPosition(0.6 + 0.6 + 0.8, Consts.base_meuble_haut)
        .setFaceMaterial(materials.red);

    // Rattrapage Meuble haut
    (new boxMur(mur_evier, 0.08, 0.7, 0.35, materials.white))
        .setPosition(0.6 + 0.6 + 0.8 + 0.6, Consts.base_meuble_haut)
        .setFaceMaterial(materials.white);

    // Angle haut
    (new boxMur(mur_evier, 0.6, 0.7, 0.35, materials.white))
        .setPosition(0.6 + 0.6 + 0.8 + 0.6 + 0.08, Consts.base_meuble_haut)
        .setFaceMaterial(materials.blue);

    /**
     * Mur four
     */
    var mur_four = new mur(spaceCuisine, 2.35 / 2, 0, 2.35, 180,
        materials.white);

    // Four 64 de profondeur ????
    (new boxMur(mur_four, 0.6, 0.6, 0.60, materials.white))
        .setPosition(0.94, Consts.hauteur_pied)
        .setFaceMaterial(materials.red);

    // tiroir à épices
    (new boxMur(mur_four, 0.15, 0.6, 0.60, materials.white))
        .setPosition(0.94 + 0.6, Consts.hauteur_pied)
        .setFaceMaterial(materials.blue);

    // interstice
    (new boxMur(mur_four, 0.06, 0.6, 0.60, materials.white))
        .setPosition(0.94 + 0.6 + 0.15, Consts.hauteur_pied)
        .setFaceMaterial(materials.white);

    // Colonne frigo
    (new boxMur(mur_four, 0.6, Consts.hauteur_colonnes, 0.6, materials.white))
        .setPosition(0.94 + 0.6 + 0.15 + 0.06, Consts.hauteur_pied)
        .setFaceMaterial(materials.red)
        .setRightMaterial(cheneMaterialGenerator.getForSurface(0.6, Consts.hauteur_colonnes))
        .setLeftMaterial(cheneMaterialGenerator.getForSurface(0.6, Consts.hauteur_colonnes));

    // Bandeau 
    (new boxMur(mur_four, 2.35, Consts.hauteur_pied, 0.55, materials.white))
        .setPosition(0, 0)
        .setFaceMaterial(materials.white);

    // Angle haut
    (new boxMur(mur_four, 0.64, 0.7, 0.35, materials.white))
        .setPosition(0, Consts.base_meuble_haut)
        .setFaceMaterial(materials.blue);

    // Module haut
    (new boxMur(mur_four, 0.3, 0.7, 0.35, materials.white))
        .setPosition(0.64, Consts.base_meuble_haut)
        .setFaceMaterial(materials.white);

    // Meuble haut haute aspirante
    (new boxMur(mur_four, 0.6, 0.7, 0.35, materials.white))
        .setPosition(0.64 + 0.3, Consts.base_meuble_haut)
        .setFaceMaterial(materials.red);

    // Meuble haut
    (new boxMur(mur_four, 0.15, 0.7, 0.35, materials.white))
        .setPosition(0.64 + 0.3 + 0.6, Consts.base_meuble_haut)
        .setFaceMaterial(materials.blue);

    // Meuble haut / interstice avec colonne frigo
    (new boxMur(mur_four, 0.06, 0.7, 0.35, materials.white))
        .setPosition(0.64 + 0.3 + 0.6 + 0.15, Consts.base_meuble_haut)
        .setFaceMaterial(materials.white);


    var plan_de_travail = 

    /**
     * Coffrage
     */
    (new boxMur(mur_evier, 3.28, Consts.hauteur_coffrage, 0.6, materials.white))
        .setPosition(0, Consts.base_coffrage)
        .setFaceMaterial(materials.white);

    (new boxMur(mur_four, 2.35, Consts.hauteur_coffrage, 0.6, materials.white))
        .setPosition(0, Consts.base_coffrage)
        .setFaceMaterial(materials.white);


    var mur_fenetre1 = new mur(spaceCuisine, 2.35 - 0.30, 3.28, 0.60, -180,
        materials.white);


    var fenetre_cuisine = new mur(spaceCuisine, 2.35 - 0.60 - 0.725 / 2, 3.28, 0.725, -180,
        materials.fenetre_cuisine);


    var mur_fenetre2 = new mur(spaceCuisine, 2.35 - 1.40, 3.28, 0.15, -180,
        materials.white);
}

export { Cuisine };