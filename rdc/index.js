import * as THREE from '../node_modules/three/build/three.module.js';
import * as Consts from '../common/consts.js';

import { newMesh, boxMur } from '../common/objects.js';
import { realSizeMaterial } from '../common/materials.js';

import Camera from '../common/camera.js';

import * as cuisine from './cuisine.js';
import * as salon from './salon.js';

/**
 * Index definition
 */
"use strict";

/**
 * Renderer setup
 */
var renderer = new THREE.WebGLRenderer();
renderer.shadowMap.enabled = true;
renderer.shadowMap.renderSingleSided = false;
renderer.setSize(window.innerWidth, window.innerHeight);
renderer.shadowMap.type = THREE.PCFSoftShadowMap;

var container = document.getElementById('container');
container.appendChild(renderer.domElement);

/**
 * Scene definition
 */
var scene = new THREE.Scene();
scene.background = new THREE.Color(0x666666);

var axisHelper = new THREE.AxisHelper(2);
scene.add(axisHelper);

/**
 * Camera definition
 */

var camera = new Camera(-2.6, -2.26, 1.8, -1.9, 0.25, 1.5);

var ambientLight = new THREE.AmbientLight(0x303030, 1); // soft white light
scene.add(ambientLight);

/**
 * Ground
 */
var carrelage = new realSizeMaterial(0.6, 0.6, 'materiaux/citara45x45_.jpg');
var sol = newMesh(
    new THREE.PlaneGeometry(9, 9),
    carrelage.getForSurface(9,9)
);
sol.position.x = -2;
sol.position.y = - Consts.longueurMurVaisselier + (6.15 / 2);

scene.add(sol);
salon.Salon(scene);
cuisine.Cuisine(scene);


function animate() {
    requestAnimationFrame(animate);
    renderer.render(scene, camera.camera);
}
animate();