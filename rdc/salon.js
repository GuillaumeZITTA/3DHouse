import * as THREE from '../node_modules/three/build/three.module.js';
import * as Consts from '../common/consts.js';
import { materials, realSizeMaterial } from '../common/materials.js';
import { deg2rad } from '../common/utils.js';
import { mur, porte } from '../common/murs.js';
import { newMesh, newLightGlobe, boxMur } from '../common/objects.js';

"use strict";

function Salon(scene) {

    /**
     * Light definition
     */
    var ampoule = newLightGlobe(scene);
    ampoule.position.set(-5.61 + 3.48 / 2, 0, 2.4);

    var cheneMaterialGenerator = new realSizeMaterial(0.4, 0.4, 'materiaux/chene.png');

    var murVaisselier = new mur(scene,
        0,
        - Consts.longueurMurVaisselier / 2,
        Consts.longueurMurVaisselier,
        -90,
        materials.red
    );

    // Colonne 
    (new boxMur(murVaisselier, 0.9, Consts.hauteur_colonnes, 0.6, materials.white))
        .setPosition(0, Consts.hauteur_pied)
        .setFaceMaterial(materials.red)
        .setLeftMaterial(cheneMaterialGenerator.getForSurface(0.6, Consts.hauteur_colonnes));

    // Colonne 
    (new boxMur(murVaisselier, 0.9, Consts.hauteur_colonnes, 0.6, materials.white))
        .setPosition(0.9, Consts.hauteur_pied)
        .setFaceMaterial(materials.blue);

    // Bandeau 
    (new boxMur(murVaisselier, 1.8, Consts.hauteur_pied, 0.55, materials.white))
        .setPosition(0, 0)
        .setFaceMaterial(materials.white);

    // Coffrage
    (new boxMur(murVaisselier, 1.85, Consts.hauteur_coffrage, 0.6, materials.white))
        .setPosition(0, Consts.base_coffrage)
        .setFaceMaterial(materials.white);

    // Angle Coffrage
    (function () {
        var largeur = Math.sin(deg2rad(40 / 2)) * 0.6 * 2;
        var profondeur = Math.cos(deg2rad(40 / 2)) * 0.6;
        var angle_coffrage = new boxMur(murVaisselier, largeur, Consts.hauteur_coffrage, profondeur, materials.white)
            .setPosition(-largeur / 4 - largeur / 2, Consts.base_coffrage);
        angle_coffrage.box.rotation.y = -deg2rad(40 / 2)
        angle_coffrage.box.position.z = Math.cos(deg2rad(40 / 2)) * (profondeur / 2);
    })();

    // Angle Bandeau
    (function () {
        var largeur = Math.sin(deg2rad(40 / 2)) * 0.55 * 2;
        var profondeur = Math.cos(deg2rad(40 / 2)) * 0.55;
        var angle_coffrage = new boxMur(murVaisselier, largeur, Consts.hauteur_pied, profondeur, materials.white)
            .setPosition(-largeur / 4 - largeur / 2, 0);
        angle_coffrage.box.rotation.y = -deg2rad(40 / 2)
        angle_coffrage.box.position.z = Math.cos(deg2rad(40 / 2)) * (profondeur / 2);
    })();
    console.log(murVaisselier);
    var murBuanderie = new mur(scene,
        -1.59 / 2,
        - Consts.longueurMurVaisselier + 1.46,
        1.59,
        0,
        materials.white
    );

    var porteBuanderie = porte(
        -1.59 + 0.8 / 2,
        - Consts.longueurMurVaisselier + 1.46,
        0.8,
        0,
        materials.porte_buanderie
    )
    porteBuanderie.rotation.z = deg2rad(180);
    scene.add(porteBuanderie);

    var murBibliotheque = new mur(scene,
        -1.59,
        - Consts.longueurMurVaisselier + 0.45,
        0.90,
        -90,
        materials.white
    );

    var murEscalier = new mur(scene,
        - 5.61 / 2,
        - Consts.longueurMurVaisselier,
        5.61,
        0,
        materials.white
    );

    var murTv = new mur(scene,
        -5.61,
        - Consts.longueurMurVaisselier + (6.15 / 2),
        6.15,
        90,
        materials.white
    );

    var murGaucheFenetre = new mur(scene,
        -5.61 + (0.85 / 2),
        - Consts.longueurMurVaisselier + 6.15,
        0.85,
        180,
        materials.white
    );

    var murFenetre = new mur(scene,
        -5.61 + 0.85 + 2.355 / 2,
        - Consts.longueurMurVaisselier + 6.15,
        2.355,
        180,
        materials.fenetre_salon
    );

    var murDroiteFenetre = new mur(scene,
        -5.61 + 0.85 + 2.355 + 0.45 / 2,
        - Consts.longueurMurVaisselier + 6.15,
        0.45,
        180,
        materials.white
    );

    var poteau = newMesh(
        new THREE.BoxGeometry(0.54, 0.56, Consts.hauteur_mur),
        materials.white
    )

    scene.add(poteau);
    poteau.position.x = -1.59 - 0.54 / 2;
    poteau.position.y = -Consts.longueurMurVaisselier + 1.46 - 0.56 / 2;
    poteau.position.z = Consts.hauteur_mur / 2;

    var angle_escalier = Math.atan(1.25 / 2.61);
    var longueur_escalier = Math.sqrt(1.25 * 1.25 + 2.61 * 2.61);
    var escalier = newMesh(
        new THREE.BoxGeometry(longueur_escalier, 0.90, 0.01),
        materials.white
    );

    console.log(['Salon: preadd', Consts.hauteur_pied]);

    scene.add(escalier);
    escalier.rotation.y = angle_escalier;
    escalier.position.x = -2.13 - 0.81 - 2.61 / 2;
    escalier.position.y = -Consts.longueurMurVaisselier + 0.9 / 2;
    escalier.position.z = 0.51 + (1.25 / 2);
}

export { Salon };