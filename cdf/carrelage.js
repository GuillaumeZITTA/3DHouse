import { Box } from '../api/v1/box.js';
import { SimpleImage } from '../api/v1/material/simple_image.js';
import { config } from '../api/v1/config.js';


var carrelages = [
    new SimpleImage("cdf/images/carrelage.png")
]

/**
 *
 *
 * @class CarrelageBox
 * @extends {Box}
 */
class CarrelageBox extends Box {
    constructor(width, height, depth, uid) {
        super(width, height, depth, carrelages[0]);

        this.uid = uid;
        var data;

        this.carrelageIndex = 0;
        if (data = config.get('carrelage')) {
            if (data[this.uid]) {
                this.carrelageIndex = data[this.uid];
            }
        }
        this.material = carrelages[this.carrelageIndex];
    }
}

/**
 *
 *
 * @param {THREE.Object3D} mur
 * @param {float} x
 * @param {float} y
 * @param {float} longueur_carreau
 * @param {float} hauteur_carreau
 * @param {*} texture
 * @param {float} taille_joint
 * @param {*} texture_joint
 * @param {float} epaisseur
 */
var carrelage = function(mur, x, y, longueur_totale, hauteur_totale, longueur_carreau, hauteur_carreau, texture, taille_joint, texture_joint, epaisseur, murId, lpc=0, hpc=0) {

    if ( hpc == 0 ) hpc=hauteur_carreau;
    if ( lpc == 0 ) lpc=longueur_carreau;

    var hauteur_courante = 0;
    var idX=0;
    var idY=0;
    while ( hauteur_courante < hauteur_totale ) {
        idY++;
        var hauteur_prochain_carreau;
        if (hauteur_courante == 0) {
            hauteur_prochain_carreau = hpc;
        } else if (hauteur_totale - hauteur_courante > hauteur_carreau) {
            hauteur_prochain_carreau = hauteur_carreau;
        } else {
            hauteur_prochain_carreau = hauteur_totale - hauteur_courante;
            console.log(murId+"-"+idY+" hauteur dernier carreau: "+hauteur_prochain_carreau);
        }

        var longueur_courante = 0;
        while ( longueur_courante < longueur_totale ) {
            idX++;
            var long_prochain_carreau;
            if (longueur_courante == 0) {
                long_prochain_carreau = lpc;
            } else if (longueur_totale - longueur_courante > longueur_carreau) {
                long_prochain_carreau = longueur_carreau;
            } else {
                long_prochain_carreau = longueur_totale - longueur_courante;
                console.log(murId+"-"+idX+" longueur dernier carreau: "+long_prochain_carreau);
            }
            let carreau = new CarrelageBox(long_prochain_carreau, hauteur_prochain_carreau, epaisseur, murId + "-" + idX + "-" + idY);
            carreau.setPosition(x+longueur_courante,y+hauteur_courante);
            mur.add(carreau);
            longueur_courante = longueur_courante +long_prochain_carreau +taille_joint;
        }
        hauteur_courante = hauteur_courante + hauteur_prochain_carreau + taille_joint;
    }

    let joint = new Box(longueur_totale, hauteur_totale, epaisseur-0.001, texture_joint);
    joint.setPosition(x,y);
    mur.add(joint);
}

export { carrelage };