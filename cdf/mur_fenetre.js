import * as THREE from '../node_modules/three/build/three.module.js';
import * as PlainMaterial from '../api/v1/material/plain.js';
import { SimpleImage } from '../api/v1/material/simple_image.js';
import { Box } from '../api/v1/box.js';
import {carrelage} from './carrelage.js';
import { c, prof_placard_droite, matiere_placards } from './mesures.js';


export default function(space){
    var murFenetre = new Box(0.05, c.l_mur_fenetre, c.hauteur_mur, PlainMaterial.white);

    murFenetre.setPosition(c.l_mur_fond, c.l_mur_gauche - c.l_mur_fenetre);
    var mur_space = murFenetre.sides.xMinus.get_space();

    var radiateur = new Box(0.876, 0.820, 0.07, PlainMaterial.white);
    radiateur.sides.zPlus.set_material(new SimpleImage("cdf/images/Radiateur.png"));
    mur_space.add(radiateur);
    radiateur.setPosition(1.839, 0.130);

    var hauteur_fenetre = 1.527;
    var fenetre = new Box(1.048, hauteur_fenetre, 0.01, PlainMaterial.white);
    fenetre.sides.zPlus.set_material(new SimpleImage("cdf/images/fenetre.png"));
    mur_space.add(fenetre);
    fenetre.setPosition(1.734, c.hauteur_mur-hauteur_fenetre);

    var l_placard_droite = 1.7;
    var placard_droite = new Box(l_placard_droite, c.hauteur_mur, prof_placard_droite - c.epaisseur_bois , matiere_placards);
    //placard_droite.setPosition(c.l_mur_fond - l_placard_fond- c.prof_placard_droite, 1.2, 0);
    mur_space.add(placard_droite);

    //enlever epaisseur carrelage ~ 1 cm
    space.add(murFenetre);
}