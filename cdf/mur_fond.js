
import * as PlainMaterial from '../api/v1/material/plain.js';
import { SimpleImage } from '../api/v1/material/simple_image.js';
import { Box } from '../api/v1/box.js';
import {carrelage} from './carrelage.js';
import { c, prof_placard_droite } from './mesures.js';

export default function(space){
    var murFond = new Box(c.l_mur_fond, 0.05, c.hauteur_mur, PlainMaterial.white);

    murFond.setPosition(0, c.l_mur_gauche);
    space.add(murFond);

    var space_mur = murFond.sides.yMinus.get_space();

    console.log(["prof_placard_droite", prof_placard_droite]);
    var placard_fond = new Box(c.l_placard_fond, c.h_placard_fond, 0.3, PlainMaterial.white);
    placard_fond.setPosition(c.l_mur_fond - c.l_placard_fond- prof_placard_droite, c.hauteur_mur-c.h_placard_fond, 0);
    placard_fond.sides.zPlus.set_material(new SimpleImage("cdf/images/placard_fond.png"));
    space_mur.add(placard_fond);

    var largeur_lit=1.4;
    var espace_cote_lit = ( c.l_placard_fond - largeur_lit ) /2;
    console.log(["espace_cote_lit", espace_cote_lit]);

    if (false) {
        var lit_deplie = new Box(largeur_lit, 0.45, 1.9, PlainMaterial.white);
        lit_deplie.setPosition(c.prof_placard_gauche + espace_cote_lit, 0, 0 );
        space_mur.add(lit_deplie);
    } else {
        var assise_lit_plie = new Box(largeur_lit, 0.45, 0.9, PlainMaterial.white);
        var dossier_lit_plie = new Box(largeur_lit, 0.45+0.6, 0.3, PlainMaterial.white);
        
        assise_lit_plie.setPosition(c.prof_placard_gauche + espace_cote_lit, 0, 0 );
        dossier_lit_plie.setPosition(c.prof_placard_gauche + espace_cote_lit, 0, 0 );
        space_mur.add(assise_lit_plie);
        space_mur.add(dossier_lit_plie);
    }


    
}