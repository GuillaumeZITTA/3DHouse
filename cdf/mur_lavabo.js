import * as THREE from '../node_modules/three/build/three.module.js';
import * as PlainMaterial from '../api/v1/material/plain.js';
import { SimpleImage } from '../api/v1/material/simple_image.js';
import { Box } from '../api/v1/box.js';
import {carrelage} from './carrelage.js';
import { c } from './mesures.js';
import { deg2rad } from '../common/utils.js';

export default function(space){
    
    //var murLavabo = new Box(0.05, c.l_mur_lavabo, c.hauteur_mur, PlainMaterial.yellow);
    //murLavabo.setPosition(-0.05, -c.l_mur_lavabo);
    //space.add(murLavabo_1);

    var murLavabo_1 = new Box(0.05, 0.14, c.hauteur_mur, PlainMaterial.white);
    murLavabo_1.setPosition(-0.05, -0.14)
    space.add(murLavabo_1);

    var h_porte = 2.03;
    var l_porte = 0.825;
    var murLavabo_dessus_porte = new Box(0.05, l_porte, c.hauteur_mur - h_porte, PlainMaterial.white);
    murLavabo_dessus_porte.setPosition(-0.05, -0.14 - l_porte, h_porte);

    var space_porte = new THREE.Object3D();
    //space_porte.position.x = -0.05;
    space_porte.position.y = -0.14;
    space_porte.rotation.z = deg2rad(-90);
    var porte = new Box(0.05, l_porte, h_porte, PlainMaterial.white);
    space_porte.add(porte);
    space.add(space_porte);

    //murLavabo_dessus_porte.position.z=c.hauteur_mur-;
    space.add(murLavabo_dessus_porte);
    //enlever epaisseur carrelage ~ 1 cm

    var murLavabo = new Box(0.05, c.l_mur_lavabo-(0.14 + l_porte), c.hauteur_mur, PlainMaterial.white);
    murLavabo.setPosition(-0.05, -c.l_mur_lavabo);
    space.add(murLavabo);

    var space_mur = murLavabo.sides.xPlus.get_space();
    var cloison1 = new Box(0.05, c.hauteur_mur, 0.49, PlainMaterial.white);
    cloison1.setPosition(c.l_mur_lavabo-(0.14 + l_porte)-0.05-0.02, 0);
    space_mur.add(cloison1);
}