
import { Box } from '../api/v1/box.js';
import { c, matiere_placards_transparent } from './mesures.js';
import * as PlainMaterial from '../api/v1/material/plain.js';

const marge = 0.003;

export default function(face, x_pos, y_pos, largeur, hauteur, nom=""){
    var porte=new Box(largeur-marge, hauteur-marge, c.epaisseur_bois, matiere_placards_transparent);
    porte.setPosition(x_pos, y_pos);
    face.add(porte);
    console.log({nom: nom, x: x_pos, y: y_pos, l: largeur-marge, h: hauteur-marge });
}
