import * as THREE from '../node_modules/three/build/three.module.js';
import * as PlainMaterial from '../api/v1/material/plain.js';
import { SimpleImage } from '../api/v1/material/simple_image.js';
import { Box } from '../api/v1/box.js';
import {carrelage} from './carrelage.js';
import { c, matiere_placards } from './mesures.js';
import porte_meuble from './porte_meuble.js';
import { deg2rad } from '../common/utils.js';

export default function(space){
    var murGauche = new Box(0.05, c.l_mur_gauche, c.hauteur_mur, PlainMaterial.white);

    murGauche.setPosition(-0.05, 0);
    //enlever epaisseur carrelage ~ 1 cm
    space.add(murGauche);

    var space_mur = murGauche.sides.xPlus.get_space();
    var longueur_placard = 1.9;

    var placard_gauche = new Box(longueur_placard, c.hauteur_mur, c.prof_placard_gauche - c.epaisseur_bois , PlainMaterial.transparent);
    placard_gauche.setPosition(c.l_mur_gauche - longueur_placard, 0, 0);
    space_mur.add(placard_gauche);

    placard_gauche.sides.zPlus.axes(2);
    var face_meuble = placard_gauche.sides.zPlus.get_space();
    face_meuble.rotation.z = deg2rad(0);
    face_meuble.position.x = face_meuble.position.x-longueur_placard;

    var hauteur_porte_bas = 0.63;
    var hauteur_plinthe = 0.1;

    porte_meuble(face_meuble, 0,   hauteur_plinthe, 0.5, hauteur_porte_bas, "porte_bas");
    porte_meuble(face_meuble, 0.5, hauteur_plinthe, 0.5, hauteur_porte_bas, "porte_bas");

    var porte_bureau_space = new THREE.Object3D();
    porte_bureau_space.position.y += hauteur_plinthe + hauteur_porte_bas;
    face_meuble.add(porte_bureau_space);
    if(true) {
        porte_bureau_space.position.y += c.epaisseur_bois;
        porte_bureau_space.rotation.x = deg2rad(90);
    }
    porte_meuble(porte_bureau_space, 0,    0, 1, 0.5, "bureau");

    var h_porte_dessus_secretaire = c.hauteur_mur - 0.36 - (hauteur_porte_bas + 0.5 +  hauteur_plinthe);
    porte_meuble(face_meuble, 0,  hauteur_porte_bas + 0.5 +  hauteur_plinthe, 0.5, h_porte_dessus_secretaire);
    porte_meuble(face_meuble, 0.5, hauteur_porte_bas + 0.5 + hauteur_plinthe, 0.5, h_porte_dessus_secretaire);
    porte_meuble(face_meuble, 0,  hauteur_porte_bas + 0.5 +  hauteur_plinthe + h_porte_dessus_secretaire, 0.5, 0.36);
    porte_meuble(face_meuble, 0.5, hauteur_porte_bas + 0.5 + hauteur_plinthe + h_porte_dessus_secretaire, 0.5, 0.36);
    var largeur_porte_penderie = longueur_placard - 1 - c.prof_placard_fond;

    //penderie
    var h_porte_haut_penderie = 0.36;
    var h_porte_bas_penderie = c.hauteur_mur - hauteur_plinthe - h_porte_haut_penderie;


    porte_meuble(face_meuble, 1, hauteur_plinthe, largeur_porte_penderie, h_porte_bas_penderie);
    porte_meuble(face_meuble, 1, hauteur_plinthe+h_porte_bas_penderie, largeur_porte_penderie, h_porte_haut_penderie);


    //Interieur meuble
    var joue = new Box(c.epaisseur_bois, c.hauteur_mur, c.prof_placard_gauche - 0.02, matiere_placards);
    joue.setPosition(c.l_mur_gauche - longueur_placard, 0, 0);
    space_mur.add(joue);

    function etagere(x, y, l, nom) {
        var x_real = ( c.l_mur_gauche - longueur_placard + c.epaisseur_bois ) + x
        var etagere_box = new Box(l, c.epaisseur_bois, c.prof_placard_gauche - 0.02, matiere_placards);
        etagere_box.setPosition(x_real, y, 0);
        space_mur.add(etagere_box);
        console.log({type:"etagere", nom: nom, x: Math.round(x*1000), y: Math.round(y*1000), l: Math.round(l*1000)});
    }
    etagere(0, 0.1,longueur_placard - c.epaisseur_bois , "plancher");

    console.log(["longueur plancher", longueur_placard - c.epaisseur_bois])

    var largeur_etagere = 1-1.5*c.epaisseur_bois;
    console.log(['largeur_etagere',largeur_etagere]);
    
    var vertical_interne = new Box(c.epaisseur_bois, c.hauteur_mur - c.epaisseur_bois - 0.1, c.prof_placard_gauche - 0.02, matiere_placards);
    console.log(['hauteur_vertical_interne', c.hauteur_mur - c.epaisseur_bois - 0.1])
    vertical_interne.setPosition(c.l_mur_gauche - longueur_placard + largeur_etagere + c.epaisseur_bois, 0.10 + c.epaisseur_bois, 0);
    space_mur.add(vertical_interne);
    
    etagere(0, 0.1 + 0.32,largeur_etagere , "etagere_bas");

    etagere(0, 0.1 + hauteur_porte_bas,largeur_etagere , "etagere_bureau");

    etagere(0, 0.1 + hauteur_porte_bas + 0.5 - 0.5 * c.epaisseur_bois,largeur_etagere , "etagere_haut_bureau");

    etagere(0, 0.1 + hauteur_porte_bas + 0.5 + h_porte_dessus_secretaire/2 ,largeur_etagere, "etagere_milieu_haut");

    etagere(0, 0.1 + hauteur_porte_bas + 0.5 + h_porte_dessus_secretaire - 0.5 * c.epaisseur_bois ,largeur_etagere, "etagere_haut");

    etagere(c.epaisseur_bois + largeur_etagere,  0.1 + hauteur_porte_bas + 0.5 + h_porte_dessus_secretaire - 0.5 * c.epaisseur_bois ,longueur_placard - largeur_etagere - 2*c.epaisseur_bois, "etagere_haut_penderie");


    console.log(['etagere_haut_penderie',longueur_placard - largeur_etagere - 2*c.epaisseur_bois]);
}
