
import * as PlainMaterial from '../api/v1/material/plain.js';
import { SimpleImage } from '../api/v1/material/simple_image.js';
import { Box } from '../api/v1/box.js';
import {carrelage} from './carrelage.js';
import { c } from './mesures.js';

export default function(space){
    var murDouche = new Box(c.l_mur_douche, 0.05, c.hauteur_mur, PlainMaterial.white);

    murDouche.setPosition(0, -0.05-c.l_mur_lavabo);
    //enlever epaisseur carrelage ~ 1 cm
    space.add(murDouche);

}