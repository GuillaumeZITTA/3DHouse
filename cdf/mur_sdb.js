
import * as PlainMaterial from '../api/v1/material/plain.js';
import { Box } from '../api/v1/box.js';
import { c } from './mesures.js';

export default function(space){
    var l_mursdb = 2.18;
    var l_segment_gauche = 0.9;
    var h_porte = 2;
    var l_porte = 0.73;

    var segment_gauche = new Box(l_segment_gauche, 0.05, c.hauteur_mur, PlainMaterial.white);
    segment_gauche.setPosition(c.l_mur_fond-l_segment_gauche, -0.03 + c.l_mur_gauche - 2.812);
    space.add(segment_gauche);

    var dessus_porte = new Box(l_porte, 0.05, c.hauteur_mur-h_porte, PlainMaterial.white);
    dessus_porte.setPosition(c.l_mur_fond-l_segment_gauche-l_porte, -0.03 + c.l_mur_gauche - 2.812, h_porte);
    space.add(dessus_porte);

    var segment_droite = new Box(l_mursdb - l_porte - l_segment_gauche, 0.05, c.hauteur_mur, PlainMaterial.white);
    segment_droite.setPosition(c.l_mur_fond-l_mursdb, -0.03 + c.l_mur_gauche - 2.812);
    space.add(segment_droite);

}