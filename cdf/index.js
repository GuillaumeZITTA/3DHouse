import * as THREE from '../node_modules/three/build/three.module.js';
import { World } from '../api/v1/world.js';
import { Camera } from '../api/v1/camera.js';
import { Box } from '../api/v1/box.js';
import { LightBulb } from '../api/v1/light/lightBulb.js';

import * as PlainMaterial from '../api/v1/material/plain.js';

import { ShadowedMesh } from '../api/v1/shadowed_mesh.js';
import { deg2rad } from '../common/utils.js';
/**
 * World definition
 */
var camera = new Camera(5,0.3,5, 1,1,1);
World.setUp(camera);

World.scene.background = new THREE.Color(0x666666);

var axesHelper = new THREE.AxisHelper(2);
//World.scene.add(axesHelper);

var ambientLight = new THREE.AmbientLight(0x404040, 2.5); // soft white light
World.scene.add(ambientLight);

var ampoule = new LightBulb();
ampoule.position.set(1, 0, 2.5);
World.scene.add(ampoule);

/**
 * Ground
 */

//var carrelage_texture = new realSizeMaterial(0.6, 0.6, 'materiaux/citara45x45_.jpg');

var sol = new Box(10,10,0.1, PlainMaterial.white)

import {carrelage} from './carrelage.js';
carrelage(World.scene,0,-5,10,10,0.6, 0.6, null,0.003,PlainMaterial.white,0.01, 1 )

//World.scene.add(sol);
console.log("index");


import mur_lavabo from './mur_lavabo.js';
mur_lavabo(World.scene);

import mur_douche from './mur_douche.js';

mur_douche(World.scene);




var spaceFond = new THREE.Object3D();
World.scene.add(spaceFond);
spaceFond.rotation.z = deg2rad(-40.5);
spaceFond.add(axesHelper);

import mur_gauche from './mur_gauche.js';
mur_gauche(spaceFond);

import mur_fond from './mur_fond.js';
mur_fond(spaceFond);

import mur_sdb from './mur_sdb.js';
mur_sdb(spaceFond);

import mur_fenetre from './mur_fenetre.js';
mur_fenetre(spaceFond);

