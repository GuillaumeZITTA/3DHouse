import * as THREE from '../node_modules/three/build/three.module.js';

var taille_module = 0.36;
export const c = {
    hauteur_mur: 2.45,
    l_mur_gauche: 2.363,
    l_mur_fond: 3.193,
    l_mur_fenetre: 3.954,
    l_mur_douche: 1.417,
    l_mur_lavabo: 3.286,
    prof_placard_gauche: 0.6,
    l_placard_fond: taille_module*6,
    h_placard_fond: taille_module*3,
    prof_placard_fond: 0.3,
    epaisseur_bois: 0.018
};

export const prof_placard_droite = c.l_mur_fond - c.prof_placard_gauche - c.l_placard_fond;

export const matiere_placards = new THREE.MeshPhongMaterial( { color: 0x758690 } );
export const matiere_placards_transparent = new THREE.MeshPhongMaterial( { color: 0x758690, opacity: 0.9, transparent: true } );
