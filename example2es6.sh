#!/bin/bash -ex

if ! [[ -d regenlib ]]; then
    mkdir regenlib
fi

cat <(echo 'import * as THREE from '\''../node_modules/three/build/three.module.js'\'';') \
    node_modules/three/examples/js/controls/OrbitControls.js \
    <(echo 'export { OrbitControls }') \
| sed 's/THREE.OrbitControls/OrbitControls/;s/^OrbitControls = function/function OrbitControls/ ' > regenlib/OrbitControls.js

cat <(echo 'import * as THREE from '\''../node_modules/three/build/three.module.js'\'';') \
    node_modules/three/examples/js/objects/Reflector.js \
    <(echo 'export { Reflector }') \
| sed 's/THREE.Reflector/Reflector/;s/^Reflector = function/function Reflector/ ' > regenlib/Reflector.js
