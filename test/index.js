import * as THREE from '../node_modules/three/build/three.module.js';
import { World } from '../api/v1/world.js';
import { Camera } from '../api/v1/camera.js';

import { LightBulb } from '../api/v1/light/lightBulb.js';
import { Box } from '../api/v1/box.js';
import * as PlainMaterial from '../api/v1/material/plain.js';

/**
 * World definition
 */
var camera = new Camera(1, 2.2, 1.8, 1.7/2, 2.2/2, 2.4/4);
World.setUp(camera);

World.scene.background = new THREE.Color(0x666666);

var axesHelper = new THREE.AxisHelper(2);
World.scene.add(axesHelper);

var ambientLight = new THREE.AmbientLight(0x404040, 2.5); // soft white light
World.scene.add(ambientLight);

var ampoule = new LightBulb();
ampoule.position.set(-1, -1, 2.4);
World.scene.add(ampoule);

//on xPlus
var xPlus1 = new Box(1,2,3,PlainMaterial.white);
xPlus1.sides.xPlus.set_material(PlainMaterial.yellow);

var xPlus2 = new Box(1, 1, 1, PlainMaterial.red);
xPlus2.sides.xPlus.set_material(PlainMaterial.green);
//xPlus1.sides.xPlus.get_space().add(xPlus2);
xPlus1.sides.xPlus.get_space().add(new THREE.AxisHelper(1));

World.scene.add(xPlus1);

//on xMinus
var xMinus1 = new Box(1,2,3,PlainMaterial.white);
xMinus1.sides.xMinus.set_material(PlainMaterial.yellow);
xMinus1.setPosition(5,0);

var xMinus2 = new Box(1, 1, 1, PlainMaterial.red);
xMinus2.sides.xPlus.set_material(PlainMaterial.green);
//xMinus1.sides.xMinus.get_space().add(xMinus2);
xMinus1.sides.xMinus.get_space().add(new THREE.AxisHelper(1));

World.scene.add(xMinus1);

//on yPlus
var yPlus1 = new Box(1,2,3,PlainMaterial.white);
yPlus1.sides.yPlus.set_material(PlainMaterial.yellow);
yPlus1.setPosition(10,0);

var yPlus2 = new Box(1, 1, 1, PlainMaterial.red);
yPlus2.sides.yPlus.set_material(PlainMaterial.green);
//yPlus1.sides.yPlus.get_space().add(yPlus2);
yPlus1.sides.yPlus.get_space().add(new THREE.AxisHelper(1));

World.scene.add(yPlus1);

//on yMinus
var yMinus1 = new Box(1,2,3,PlainMaterial.white);
yMinus1.sides.yMinus.set_material(PlainMaterial.yellow);
yMinus1.setPosition(15,0);

var yMinus2 = new Box(1, 1, 1, PlainMaterial.red);
yMinus2.sides.yPlus.set_material(PlainMaterial.green);
//yMinus1.sides.yMinus.get_space().add(yMinus2);
yMinus1.sides.yMinus.get_space().add(new THREE.AxisHelper(1));

World.scene.add(yMinus1);

//on zPlus
var zPlus1 = new Box(1,2,3,PlainMaterial.white);
zPlus1.sides.zPlus.set_material(PlainMaterial.yellow);
zPlus1.setPosition(20,0);

var zPlus2 = new Box(1, 1, 1, PlainMaterial.red);
zPlus2.sides.zPlus.set_material(PlainMaterial.green);
//zPlus1.sides.zPlus.get_space().add(zPlus2);
zPlus1.sides.zPlus.get_space().add(new THREE.AxisHelper(2));

World.scene.add(zPlus1);

//on zMinus
var zMinus1 = new Box(1,2,3,PlainMaterial.white);
zMinus1.sides.zMinus.set_material(PlainMaterial.yellow);
zMinus1.setPosition(25,0);

var zMinus2 = new Box(1, 1, 1, PlainMaterial.red);
zMinus2.sides.zPlus.set_material(PlainMaterial.green);
//zMinus1.sides.zMinus.get_space().add(zMinus2);
zMinus1.sides.zMinus.get_space().add(new THREE.AxisHelper(2));

World.scene.add(zMinus1);

