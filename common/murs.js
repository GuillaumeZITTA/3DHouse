import * as THREE from '../node_modules/three/build/three.module.js';

import * as Consts from './consts.js';
import { deg2rad } from './utils.js';
import { newMesh, boxMur } from './objects.js';

'use strict';

/**
 * Creates a wall
 * 
 * @param {THREE.Object3D} parent 
 * @param {float} x 
 * @param {float} y 
 * @param {float} longueur 
 * @param {float} angle 
 * @param {THREE.Material} material 
 */

function mur(parent, x, y, longueur, angle, material) {
    this.parent = parent;
    this.x = x;
    this.y = y;
    this.longueur = longueur;
    this.angle = angle;
    this.material = material;

    this.spaceMur = new THREE.Object3D();
    this.spaceMur.position.x = x;
    this.spaceMur.position.y = y;
    this.spaceMur.position.z = Consts.hauteur_mur / 2;
    this.spaceMur.rotation.x = deg2rad(90);

    //this.angle = Math.atan2(Math.abs(y1 - y2), Math.abs(x1 - x2));
    this.spaceMur.rotation.y = deg2rad(angle);
    //console.log(angle);

    this.mur = newMesh(
        //new THREE.BoxGeometry(longueur, Consts.hauteur_mur, 0.01),
        new THREE.PlaneGeometry(longueur, Consts.hauteur_mur),
        material
    );
    this.spaceMur.add(this.mur);
    parent.add(this.spaceMur);
}

/**
 * Creates a Wall
 * 
 * @param {float} x 
 * @param {float} y 
 * @param {float} longueur 
 * @param {float} angle 
 * @param {THREE.Material} material 
 * @returns {THREE.Object3D} 
 */
function porte(x, y, longueur, angle, material) {
    var hauteur_porte = 2.04;
    var spacePorte = new THREE.Object3D();
    spacePorte.position.x = x;
    spacePorte.position.y = y;
    spacePorte.position.z = hauteur_porte / 2;
    spacePorte.rotation.x = deg2rad(-90);
    spacePorte.rotation.y = deg2rad(angle);

    var porte = newMesh(
        new THREE.BoxGeometry(longueur, hauteur_porte, 0.02),
        material
    );
    spacePorte.add(porte);
    return spacePorte;
}

export { mur, porte }