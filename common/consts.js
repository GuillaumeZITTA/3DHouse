'use strict';

export const hauteur_mur = 2.46 - 0.02;
export const hauteur_colonnes = 2.04;
export const hauteur_meubles_bas = 0.72;
export const hauteur_meubles_haut = 0.72;
export const hauteur_pied = 0.12;
export const epaisseur_plan_de_travail = 0.02;
export const base_meuble_haut = hauteur_pied + hauteur_colonnes - hauteur_meubles_haut;
export const base_coffrage = hauteur_colonnes + hauteur_pied;
export const hauteur_coffrage = hauteur_mur - base_coffrage;

export const longueurMurVaisselier = 2.35 + 0.97 + 0.05;