import * as THREE from '../node_modules/three/build/three.module.js';

import * as Consts from './consts.js';
import { mur } from './murs.js';
import { materials } from './materials.js';


/**
 * Creates a "tuned" mesh
 * 
 * @param {THREE.Geometry} geometry 
 * @param {THREE.Material} material 
 * @returns {THREE.Mesh}
 */
function newMesh(geometry, material) {
    var mesh = new THREE.Mesh(geometry, material);
    mesh.receiveShadow = true;
    mesh.castShadow = true;
    return mesh;
}

/**
 * Create a lightglobe
 * 
 * @param {THREE.Object3D} parent 
 * @returns {THREE.PointLight}
 */
function newLightGlobe(parent) {
    var pointLight = new THREE.PointLight(0x777777, 1, 10);
    pointLight.shadow.camera.near = 1;
	pointLight.shadow.camera.far = 500;
    pointLight.shadow.bias = -0.00001;
    pointLight.castShadow = true;            // default false
    pointLight.shadow.mapSize.width = 1024;
    pointLight.shadow.mapSize.height = 1024;
    parent.add(pointLight);

    var pointLightHelper = new THREE.PointLightHelper( pointLight, 0.1 );
    parent.add(pointLightHelper);

    return pointLight;
}

/**
 * Represent a side of a box
 *
 * @param {boxMur} parent_box
 * @param {integer} number
 * @param {THREE.Material} material
 */

function boxSide(parent_box, number) {
    var self = this;

    this.parent_box = parent_box;
    this.number = number;
    this.material = this.parent_box.materials[self.number];

    this.set_material = function(material) {
        this.material = material;
        this.parent_box.materials[self.number] = material;
    }
}

/**
 * Creates a box on the wall
 * 
 * @param {mur} parentMur 
 * @param {float} l 
 * @param {float} h 
 * @param {float} p 
 * @param {THREE.Material} default_material 
 * @param {integer} portes 
 */
function boxMur(parentMur, l, h, p, default_material, portes = 0) {
    this.l = l;
    this.h = h;
    this.p = p;

    var self = this;

    this.materials = [
        default_material, // +x
        default_material, // -x
        default_material, // +y
        default_material, // -y
        default_material, // face
        default_material  // back
    ]

    self.sides = [];

    this.get_side = function(number) {
        if (!(self.sides[number])) {
            self.sides[number] = new boxSide(self,number);
        }
        return self.sides[number];
    }

    this.get_x_plus_side = function() {
        return self.get_side(0);
    }

    this.setRightMaterial = function (material) {
        self.get_x_plus_side().set_material(material);
        return self;
    }

    this.get_x_minus_side = function() {
        return self.get_side(1);
    }

    this.setLeftMaterial = function (material) {
        self.get_x_minus_side().set_material(material);
        return self;
    }

    this.get_y_plus_side = function() {
        return self.get_side(2);
    }

    this.setUpMaterial = function (material) {
        self.get_y_plus_side().set_material(material);
        return self;
    }

    this.get_y_minus_side = function() {
        return self.get_side(3);
    }

    this.setBottomMaterial = function (material) {
        self.get_y_minus_side().set_material(material);
        return self;
    }

    this.get_z_plus_side = function() {
        return self.get_side(4);
    }

    this.setFaceMaterial = function (material) {
        self.get_z_plus_side().set_material(material);
        return self;
    }

    this.get_z_minus_side = function() {
        return self.get_side(5);
    }

    this.setBackMaterial = function (material) {
        self.get_z_minus_side().set_material(material);
        return self;
    }

    this.setPosition = function (x, y) {
        self.box.position.x += x;
        self.box.position.y += y;
        return self;
    };

    this.box = newMesh(
        new THREE.BoxGeometry(l, h, p),
        self.materials
    );

    self.box.position.x = -parentMur.longueur / 2 + l / 2;
    self.box.position.y = -Consts.hauteur_mur / 2 + h / 2;
    self.box.position.z = p / 2;
    parentMur.mur.add(this.box);
}

export { newMesh, newLightGlobe, boxMur };