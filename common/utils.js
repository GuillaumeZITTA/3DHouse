"use strict";

/**
 * Coordinate rotation on a plane
 * 
 * @param {float} x 
 * @param {float} y 
 * @param {float} angle 
 * @returns {Array.float}
 */
function rotate(x, y, angle) {
    var angle_r = angle * ( Math.PI / 180 );
    var new_x = x * Math.cos(angle_r) - y * Math.sin(angle_r);
    var new_y = x * Math.sin(angle_r) + y * Math.cos(angle_r);
    return [new_x, new_y];
}

/**
 * Convert angle from deg to rad
 * 
 * @param {float} angle 
 * @returns {float}
 */
function deg2rad(angle) {
    return angle * ( Math.PI / 180 );
}

var data_store = new Object();

data_store.set = function(key, data) {
    var stored_data = this.getAll(key);
    stored_data[key] = data;
    location.hash = JSON.stringify(stored_data);
};

data_store.get = function(key) {
    if (key in this.getAll()) {
        return this.getAll()[key];
    } else {
        return undefined;
    }
}

data_store.getAll = function() {
    var indexOfHash = document.URL.indexOf('#');
    if ( indexOfHash < 0 ) {
        return {};
    }
    var hashString = document.URL.substr(document.URL.indexOf('#')+1);
    try {
        return JSON.parse(decodeURIComponent(hashString));        
    } catch (e) {
        console.log(['hash is not json', hashString ]);
        return undefined;
    }
    
}

export { data_store, rotate, deg2rad }