import * as THREE from '../node_modules/three/build/three.module.js';

'use strict';

/**
 * generate materials with scaled texture
 * 
 * @param {float} x 
 * @param {float} y 
 * @param {string} image 
 */
function realSizeMaterial(x, y, image) {
    var self = this;
    this.x = x;
    this.y = y;
    this.image = image;
    
    this.cache = {};

    this.getForSurface = function (x,y) {
        if (!(x+'x'+y in self.cache)) {
            var texture = new THREE.TextureLoader().load(image);
            texture.wrapS = THREE.RepeatWrapping;
            texture.wrapT = THREE.RepeatWrapping;
            texture.repeat.set(x/self.x, y/self.y);
            self.cache[x+'x'+y] = new THREE.MeshPhongMaterial( { map: texture } );
            self.cache[x+'x'+y].shininess = 0;
        } else {
            console.log('cache!');
        }
        return self.cache[x+'x'+y];
    }
}

/**
 * Global textures
 */

var textures = {
    fenetre_salon: new THREE.TextureLoader().load( 'materiaux/fenetre_salon_lapeyre.jpg' ),
    fenetre_cuisine: new THREE.TextureLoader().load( 'materiaux/fenetre_cuisine_lapeyre.jpg' ),
    porte_buanderie: new THREE.TextureLoader().load( 'materiaux/porte.jpg' ),
    tiroirs_meubles_sdb: new THREE.TextureLoader().load( 'materiaux/tiroirs_meubles_sdb.jpg' ),
    dessus_vasque_sdb: new THREE.TextureLoader().load( 'materiaux/dessus_vasque_sdb.jpg'),
    dessus_baignoire_sdb: new THREE.TextureLoader().load( 'materiaux/baignoire.jpg'),
    fenestron_sdb: new THREE.TextureLoader().load( 'materiaux/fenestron_sdb.png'),
    seche_serviette: new THREE.TextureLoader().load( 'materiaux/seche_serviette.png')
};

var materials = {
    white: new THREE.MeshPhongMaterial( { color: 0xf0f0f0 } ),
    red: new THREE.MeshPhongMaterial( { color: 0xf00000 } ),
    blue: new THREE.MeshPhongMaterial( { color: 0x0000f0 } ),
    fenetre_salon: new THREE.MeshPhongMaterial( { map: textures.fenetre_salon } ),
    fenetre_cuisine: new THREE.MeshPhongMaterial( { map: textures.fenetre_cuisine } ),
    porte_buanderie: new THREE.MeshPhongMaterial( { map: textures.porte_buanderie } ),
    tiroirs_meubles_sdb: new THREE.MeshPhongMaterial( { map: textures.tiroirs_meubles_sdb } ),
    dessus_vasque_sdb: new THREE.MeshPhongMaterial( { map: textures.dessus_vasque_sdb } ),
    dessus_baignoire_sdb: new THREE.MeshPhongMaterial( { map: textures.dessus_baignoire_sdb } ),
    fenestron_sdb: new THREE.MeshPhongMaterial( { map: textures.fenestron_sdb } ),
    seche_serviette: new THREE.MeshPhongMaterial( { map: textures.seche_serviette } ),
};

materials.white.shininess = 0;

export { materials, realSizeMaterial }