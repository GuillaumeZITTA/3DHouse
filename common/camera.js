import * as THREE from '../node_modules/three/build/three.module.js';
import { OrbitControls } from '../regenlib/OrbitControls.js';

import { data_store } from './utils.js';

'use strict';

function Camera(pos_x, pos_y , pos_z, vis_x, vis_y, vis_z) {
    var self = this;
    this.camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 1000 );
    this.camera.up.set(0,0,1);

    this.save = function () {
        var data = {};
        data.position = {};
        data.position.x = this.camera.position.x;
        data.position.y = this.camera.position.y;
        data.position.z = this.camera.position.z;

        data.target = {};
        data.target.x = this.controls.target.x;
        data.target.y = this.controls.target.y;
        data.target.z = this.controls.target.z;
        data_store.set('camera', data);
    
    }

    this.restore = function () {
        var data = data_store.get('camera');
        this.camera.position.x = data.position.x;
        this.camera.position.y = data.position.y;
        this.camera.position.z = data.position.z;

        this.controls.target.x = data.target.x;
        this.controls.target.y = data.target.y;
        this.controls.target.z = data.target.z;

        this.controls.update();
    }
    
    this.controls = new OrbitControls( this.camera );

    if (data_store.get('camera')) {
        this.restore();
    } else {
        //default position
        this.camera.position.x = pos_x;
        this.camera.position.y = pos_y;
        this.camera.position.z = pos_z;

        this.controls.target = new THREE.Vector3( vis_x, vis_y, vis_z );
        this.controls.update();
    }   

    document.getElementById("save_camera").onclick = function() {
        self.save();
    }
}

export default Camera;