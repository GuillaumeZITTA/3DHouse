import * as THREE from '../../../node_modules/three/build/three.module.js';
import { Box } from '../box.js';
import { deg2rad } from '../../../common/utils.js';

/**
 * Represents a Side of a Box that can be a new space for other objects
 *
 * @export
 * @class BoxSide
 */
export class BoxSide {

    /**
     *Creates an instance of BoxSide.
     * @param {Box} box
     * @param {integer} number
     * @memberof BoxSide
     */
    constructor(box, number) {
        this.space = null;
        this.box = box;
        this.number = number;
        this.material = this.box.material[this.number];
    }

    set_material(material) {
        this.material = material;
        this.box.material[this.number] = material;
    }

    get_space() {
        if (this.space == null) {
            this.space = new THREE.Object3D();
            this.box.add(this.space);

            if (this.number == 0) {
                //xPlus
                this.space.position.x += this.box.width/2;
                this.space.position.y -= this.box.height/2;
                this.space.position.z -= this.box.depth/2;
                this.space.rotation.z = deg2rad(90);
                this.space.rotation.y = deg2rad(90);
            } else if (this.number == 1) {
                //xMinus
                this.space.position.x -= this.box.width/2;
                this.space.position.y += this.box.height/2;
                this.space.position.z -= this.box.depth/2;
                this.space.rotation.y = deg2rad(-90);
                this.space.rotation.z = deg2rad(-90);
            } else if (this.number == 2) {
                //yPlus
                this.space.position.x += this.box.width/2;
                this.space.position.y += this.box.height/2;
                this.space.position.z -= this.box.depth/2;
                this.space.rotation.z = deg2rad(180);
                this.space.rotation.x = deg2rad(-90);
            } else if (this.number == 3) {
                //yMinus
                this.space.position.x -= this.box.width/2;
                this.space.position.y -= this.box.height/2;
                this.space.position.z -= this.box.depth/2;
                this.space.rotation.x = deg2rad(90);
            } else if (this.number == 4) {
                //zPlus
                this.space.position.x += this.box.width/2;
                this.space.position.y -= this.box.height/2;
                this.space.position.z += this.box.depth/2;
                this.space.rotation.z = deg2rad(90);
            } else if (this.number == 5) {
                //zMinus
                this.space.position.x -= this.box.width/2;
                this.space.position.y -= this.box.height/2;
                this.space.position.z -= this.box.depth/2;
                this.space.rotation.z = deg2rad(-90);
                this.space.rotation.x = deg2rad(180);
            }
        }
        return this.space;
    }

    axes(size) {
        this.get_space().add(new THREE.AxisHelper(size));
    }
}