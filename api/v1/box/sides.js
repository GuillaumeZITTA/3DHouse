import { Box } from '../box.js';
import { BoxSide } from './side.js';

const FACENUMBER = [
    "xPlus",
    "xMinus",
    "yPlus",
    "yMinus",
    "zPlus",
    "zMinus"
];

/**
 * 
 *
 * @export
 * @class Sides
 */
export class BoxSides {

    /**
     *Creates an instance of Sides.
     * @param {Box} box
     * @memberof Sides
     */
    constructor(box){
        this.box = box;
        this.sides = [];
    }

    /**
     *
     * @returns {BoxSide}
     * @readonly
     * @memberof BoxSides
     */
    get xPlus(){
        return this.getSide(0);
    };

    /**
     *
     * @returns {BoxSide}
     * @readonly
     * @memberof BoxSides
     */
    get xMinus(){
        return this.getSide(1);
    };

    /**
     *
     * @returns {BoxSide}
     * @readonly
     * @memberof BoxSides
     */
    get yPlus(){
        return this.getSide(2);
    };

    /**
     *
     * @returns {BoxSide}
     * @readonly
     * @memberof BoxSides
     */
    get yMinus(){
        return this.getSide(3);
    };

    /**
     *
     * @returns {BoxSide}
     * @readonly
     * @memberof BoxSides
     */
    get zPlus(){
        return this.getSide(4);
    };

    /**
     *
     * @returns {BoxSide}
     * @readonly
     * @memberof BoxSides
     */
    get zMinus(){
        return this.getSide(5);
    };
    
    getSide(number) {
        if (!(this.sides[number])) {
            this.sides[number] = new BoxSide(this.box, number);
        }
        return this.sides[number];
    };
}