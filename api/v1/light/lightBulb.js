import * as THREE from '../../../node_modules/three/build/three.module.js';

/**
 * Simple light emulating a light bulb
 *
 * @class LightBulb
 * @extends {THREE.PointLight}
 */
class LightBulb extends THREE.PointLight {
    constructor() {
        super(0xffffff, 0.5, 30, 2);

        this.shadow.camera.near = 1;
        this.shadow.camera.far = 500;
        this.shadow.bias = -0.00001;
        this.castShadow = true;
        this.shadow.mapSize.width = 1024;
        this.shadow.mapSize.height = 1024;

        var pointLightHelper = new THREE.PointLightHelper( this, 0.1 );
        this.add(pointLightHelper);
    }
}

export { LightBulb };