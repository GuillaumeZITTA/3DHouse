import * as THREE from '../../node_modules/three/build/three.module.js';

let scene;
let camera;
let renderer;
let container;

export class World {
    static get scene() {
        return scene;
    }

    static get renderer() {
        return renderer;
    }

    static setUp(cameraParam) {
        console.log("World.setUp");
        scene = new THREE.Scene();
        camera = cameraParam;

        renderer = new THREE.WebGLRenderer();
        renderer.shadowMap.enabled = true;
        renderer.shadowMap.renderSingleSided = false;
        renderer.shadowMap.type = THREE.PCFSoftShadowMap;

        container = document.getElementById('container');
        container.appendChild(renderer.domElement);

        container.addEventListener('click', function(event){

            if ( event.ctrlKey ) {
                event.preventDefault();

                var bounds = this.getBoundingClientRect();
                var x = event.clientX - bounds.left;
                var y = event.clientY - bounds.top;

                var mouse = new THREE.Vector2( 
                    ( x / window.innerWidth ) * 2 - 1,
                    - ( y / window.innerHeight ) * 2 + 1
                );

                var raycaster = new THREE.Raycaster();
                raycaster.setFromCamera( mouse, camera );

                var intersects = raycaster.intersectObjects( scene.children, true );
                for (var i=0; i<intersects.length; i++) {
                    if (intersects[i].object.type == 'Mesh' && 'onClick' in intersects[i].object) {
                        intersects[i].object.onClick();
                    }
                } 
            }
        });

        function animate() {
            var containerRec = container.getBoundingClientRect();
            renderer.setSize(containerRec.width, containerRec.height);
            camera.aspect = containerRec.width / containerRec.height;
            camera.updateProjectionMatrix();

            requestAnimationFrame(animate);
            renderer.render(scene, camera);
        }
    
        animate();
    }
}
