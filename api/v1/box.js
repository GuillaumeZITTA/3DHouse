import * as THREE from '../../node_modules/three/build/three.module.js';
import { ShadowedMesh } from './shadowed_mesh.js';
import { BoxSides } from './box/sides.js';

/**
 * Represent a Box
 *
 * @export
 * @class Box
 * @extends {ShadowedMesh}
 */
export class Box extends ShadowedMesh {
    /**
     *Creates an instance of Box.
     * @param {integer} width
     * @param {integer} height
     * @param {integer} depth
     * @param {THREE.Material} default_material
     * @memberof Box
     */
    constructor(width, height, depth, default_material) {
        super(
            new THREE.BoxGeometry(width, height, depth),
            [
                default_material,
                default_material,
                default_material,
                default_material,
                default_material,
                default_material // back
            ]
        );
        this.width = width;
        this.height = height;
        this.depth = depth;

        this.sidesObject = null; 

        this.position.x = this.width / 2;
        this.position.y = this.height / 2;
        this.position.z = this.depth / 2;


    }

    setPosition(x, y, z=0) {
        this.position.x += x;
        this.position.y += y;
        this.position.z += z;
        return this;
    };
 
    get sides() {
        if (this.sidesObject == null) {
            this.sidesObject = new BoxSides(this);
        }
        return this.sidesObject;
    };

    onClick() {
        console.log("click!");
    }
}
