import * as THREE from '../../node_modules/three/build/three.module.js';
import { OrbitControls } from '../../regenlib/OrbitControls.js';
import { config } from './config.js';

export class Camera extends THREE.PerspectiveCamera {
    constructor(pos_x, pos_y , pos_z, vis_x, vis_y, vis_z){
        super(75, window.innerWidth / window.innerHeight, 0.1, 1000);
        this.up.set(0,0,1);

        this.controls = new OrbitControls( this );

        var self = this;

        if (config.get('camera')) {
            this.restore();
        } else {
            //default position
            this.position.x = pos_x;
            this.position.y = pos_y;
            this.position.z = pos_z;
    
            this.controls.target = new THREE.Vector3( vis_x, vis_y, vis_z );
            this.controls.update();
        } 

        document.getElementById("save_camera").onclick = function() {
            self.save();
        }
    }

    save() {
        var data = {};
        data.position = {};
        data.position.x = this.position.x;
        data.position.y = this.position.y;
        data.position.z = this.position.z;

        data.target = {};
        data.target.x = this.controls.target.x;
        data.target.y = this.controls.target.y;
        data.target.z = this.controls.target.z;
        config.set('camera', data);
    }

    restore() {
        var data = config.get('camera');
        this.position.x = data.position.x;
        this.position.y = data.position.y;
        this.position.z = data.position.z;

        this.controls.target.x = data.target.x;
        this.controls.target.y = data.target.y;
        this.controls.target.z = data.target.z;

        this.controls.update();
    }
}