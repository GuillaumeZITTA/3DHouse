import * as THREE from '../../node_modules/three/build/three.module.js';

export class ShadowedMesh extends THREE.Mesh {
    constructor(geometry, material) {
        super(geometry, material);
        this.receiveShadow = true;
        this.castShadow = true;
    }
}