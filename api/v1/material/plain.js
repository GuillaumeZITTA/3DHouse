import * as THREE from '../../../node_modules/three/build/three.module.js';

export const white = new THREE.MeshPhongMaterial( { color: 0xf0f0f0 } );
export const grey = new THREE.MeshPhongMaterial( { color: 0xa0a0a0 } );
export const red = new THREE.MeshPhongMaterial( { color: 0xf00000 } );
export const blue = new THREE.MeshPhongMaterial( { color: 0x0000f0 } );
export const green = new THREE.MeshPhongMaterial( { color: 0x00f000 } );
export const yellow = new THREE.MeshPhongMaterial( { color: 0xf0f000 } );
export const transparent = new THREE.MeshPhongMaterial( { color: 0x0, opacity: 0,  transparent: true, } );