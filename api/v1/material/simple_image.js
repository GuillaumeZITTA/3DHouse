import * as THREE from '../../../node_modules/three/build/three.module.js';

export class SimpleImage extends THREE.MeshPhongMaterial {
    constructor(image){
        var texture = new THREE.TextureLoader().load(image);
        super({ map: texture });
    }
}