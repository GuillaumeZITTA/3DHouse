export var config = new Object();

config.set = function(key, data) {
    var stored_data = this.getAll(key);
    stored_data[key] = data;
    location.hash = JSON.stringify(stored_data);
};

config.get = function(key) {
    if (key in this.getAll()) {
        return this.getAll()[key];
    } else {
        return undefined;
    }
}

config.getAll = function() {
    var indexOfHash = document.URL.indexOf('#');
    if ( indexOfHash < 0 ) {
        return {};
    }
    var hashString = document.URL.substr(document.URL.indexOf('#')+1);
    try {
        return JSON.parse(decodeURIComponent(hashString));        
    } catch (e) {
        console.log(['hash is not json', hashString ]);
        return undefined;
    }
    
}
