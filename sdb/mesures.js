export const c = {
    hauteur_mur: 2.45,
    largeur_murs_cotes: 2.23,
    largeur_mur_baginoire: 1.7,

    longueur_baignoire: 1.5,
    largeur_baignoire: 0.7,
    hauteur_baignoire: 0.51,

    largeur_coffrage: 0.795,
    profondeur_coffrage: 0.195,
    hauteur_coffrage: 1.105,

    hauteur_meuble: 0.64,
    largeur_meuble: 1.4, //depuis mur droit 1.415
    largeur_vasque: 1.43,
    hauteur_vasque: 0.06,
    hauteur_sous_meuble: 0.26
};

export const hauteur_sur_vasque = c.hauteur_sous_meuble + c.hauteur_meuble + c.hauteur_vasque;



