import { World } from '../api/v1/world.js';
import * as PlainMaterial from '../api/v1/material/plain.js';
import { SimpleImage } from '../api/v1/material/simple_image.js';
import { Box } from '../api/v1/box.js';
import {carrelage} from './carrelage.js';
import { c } from './mesures.js';

export default function(){
    var murBaignoire = new Box(c.largeur_mur_baginoire, 0.05, c.hauteur_mur, PlainMaterial.white);
    murBaignoire.setPosition(0, -0.05);
    World.scene.add(murBaignoire);

    var space_mur = murBaignoire.sides.yPlus.get_space()

    var baignoire = new Box( c.longueur_baignoire, c.hauteur_baignoire, c.largeur_baignoire, PlainMaterial.white);
    space_mur.add(baignoire);
    baignoire.sides.yPlus.set_material(new SimpleImage("sdb/images/baignoire.jpg"));

    var habillage_baignoire = new Box(c.hauteur_baignoire, c.longueur_baignoire, 0.1, PlainMaterial.white);
    baignoire.sides.zPlus.get_space().add(habillage_baignoire);

    const hauteur_conduit=0.225;
    var conduit = new Box(c.largeur_mur_baginoire, hauteur_conduit, 0.425, PlainMaterial.white);
    space_mur.add(conduit);
    conduit.setPosition(0, c.hauteur_mur-hauteur_conduit);

    var fenetre = new Box(0.75, 0.70, 0.01, PlainMaterial.white);
    space_mur.add(fenetre);
    fenetre.setPosition(0.03,c.hauteur_mur-hauteur_conduit-0.70);
    fenetre.sides.zPlus.set_material(new SimpleImage("sdb/images/fenestron_sdb.png"));

    carrelage(space_mur, 0, c.hauteur_baignoire, c.largeur_mur_baginoire, c.hauteur_mur-c.hauteur_baignoire-hauteur_conduit, 0.15, 0.15, PlainMaterial.white, 0.002, PlainMaterial.white, 0.005, "mur_baignoire", 0.15 - 0.015 );
}