import { World } from '../api/v1/world.js';
import * as PlainMaterial from '../api/v1/material/plain.js';
import { Box } from '../api/v1/box.js';
import {carrelage} from './carrelage.js';
import { c, hauteur_sur_vasque } from './mesures.js';

export default function(){
    var murInterrupteur = new Box(0.63, 0.05, c.hauteur_mur,PlainMaterial.white);
    murInterrupteur.setPosition(0, c.largeur_murs_cotes);
    World.scene.add(murInterrupteur);

    var space_mur = murInterrupteur.sides.yMinus.get_space()

    carrelage(space_mur, 0, hauteur_sur_vasque, 0.63, c.hauteur_mur-hauteur_sur_vasque, 0.15, 0.15, PlainMaterial.white, 0.002, PlainMaterial.white, 0.005, "mur_interrupteur" );
}