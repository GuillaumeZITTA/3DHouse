import { World } from '../api/v1/world.js';
import * as PlainMaterial from '../api/v1/material/plain.js';
import { SimpleImage } from '../api/v1/material/simple_image.js';
import { Box } from '../api/v1/box.js';
import { carrelage } from './carrelage.js';
import { c, hauteur_sur_vasque } from './mesures.js';

export default function(){

// bouche à 122x29 depuis plafond 17.5 diam
// sortie fil 103x62
// barre 77x17

var murMirroir = new Box(0.05, c.largeur_murs_cotes, 2.45, PlainMaterial.white);
murMirroir.setPosition(-0.05,0);
World.scene.add(murMirroir);
var space_mur = murMirroir.sides.xPlus.get_space();

var coffrage = new Box(c.largeur_coffrage, c.hauteur_coffrage, c.profondeur_coffrage, PlainMaterial.white);
space_mur.add(coffrage);

coffrage.sides.zPlus.axes(1);
carrelage(coffrage.sides.yPlus.get_space(), 0, 0, c.largeur_coffrage, c.profondeur_coffrage, 0.15, 0.15, PlainMaterial.white, 0.002, PlainMaterial.white, 0.005, "dessus_coffrage", 0.0, 0.043 );
carrelage(coffrage.sides.zPlus.get_space(), c.hauteur_baignoire, 0, c.hauteur_coffrage-c.hauteur_baignoire, c.largeur_coffrage ,0.15, 0.15, PlainMaterial.white, 0.002, PlainMaterial.white, 0.005, "face_coffrage", 0, 0 );
carrelage(coffrage.sides.xPlus.get_space(), hauteur_sur_vasque,0, c.hauteur_coffrage-hauteur_sur_vasque, c.profondeur_coffrage,0.15, 0.15, PlainMaterial.white, 0.002, PlainMaterial.white, 0.005, "cote_coffrage");
/*
var hauteur_mur = 2.05;
var mur = new Box(0.1, 2.05, 0.49, PlainMaterial.white);
mur.setPosition(0.7,0);

space_mur.add(mur);
*/
var decalage_meuble_vasque = (c.largeur_vasque-c.largeur_meuble)/2;
//console.log(decalage_meuble_vasque);

var meubleVasque = new Box( c.largeur_meuble, c.hauteur_meuble, 0.49, PlainMaterial.white);
meubleVasque.sides.zPlus.set_material(new SimpleImage("sdb/images/tiroirs_meubles_sdb.jpg"));
meubleVasque.setPosition(
    c.largeur_murs_cotes-c.largeur_meuble-decalage_meuble_vasque,
    c.hauteur_sous_meuble
);
space_mur.add(meubleVasque);

var vasque = new Box( c.largeur_vasque, 0.49, 0.06, PlainMaterial.white);
vasque.sides.zPlus.set_material(new SimpleImage("sdb/images/dessus_vasque_sdb.jpg"));
meubleVasque.sides.yPlus.get_space().add(vasque);
vasque.setPosition(-decalage_meuble_vasque,0);

carrelage(space_mur, 0, hauteur_sur_vasque, 2.23, c.hauteur_mur-hauteur_sur_vasque, 0.15, 0.15, PlainMaterial.white, 0.003, PlainMaterial.white, 0.005, "mur_mirroir", 0.035, 0.001 );

var hauteur_mirror = c.hauteur_mur - hauteur_sur_vasque - 0.62; //0.62: hauteur depuis plafind
var mirroir = new Box(c.largeur_vasque, hauteur_mirror, 0.01, PlainMaterial.white);
mirroir.setPosition(0.8, hauteur_sur_vasque);
space_mur.add( mirroir );

}