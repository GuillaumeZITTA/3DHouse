import * as THREE from '../node_modules/three/build/three.module.js';
import { World } from '../api/v1/world.js';
import { Camera } from '../api/v1/camera.js';

import { LightBulb } from '../api/v1/light/lightBulb.js';

import { realSizeMaterial } from '../common/materials.js';

import { ShadowedMesh } from '../api/v1/shadowed_mesh.js';

/**
 * World definition
 */
var camera = new Camera(1, 2.2, 1.8, 1.7/2, 2.2/2, 2.4/4);
World.setUp(camera);

World.scene.background = new THREE.Color(0x666666);

var axesHelper = new THREE.AxisHelper(2);
World.scene.add(axesHelper);

var ambientLight = new THREE.AmbientLight(0x404040, 2.5); // soft white light
World.scene.add(ambientLight);

var ampoule = new LightBulb();
ampoule.position.set(1.70 / 2, 2.2 /2, 2.4);
World.scene.add(ampoule);

/**
 * Ground
 */
var carrelage = new realSizeMaterial(0.6, 0.6, 'materiaux/citara45x45_.jpg');
var sol = new ShadowedMesh(
    new THREE.PlaneGeometry(9,9),
    carrelage.getForSurface(9,9)
);

World.scene.add(sol);
console.log("index");

import mur_gauche from './mur_gauche.js';
mur_gauche();

import mur_baignoire from './mur_baignoire.js';
mur_baignoire();

import mur_mirroir from './mur_mirroir.js';
mur_mirroir();

import mur_interrupteur from './mur_interrupteur.js';
mur_interrupteur();

 