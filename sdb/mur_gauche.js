import { World } from '../api/v1/world.js';
import * as PlainMaterial from '../api/v1/material/plain.js';
import { SimpleImage } from '../api/v1/material/simple_image.js';
import { Box } from '../api/v1/box.js';
import {carrelage} from './carrelage.js';
import { c } from './mesures.js';

export default function(){
    var murGauche = new Box(0.05, c.largeur_murs_cotes, c.hauteur_mur, PlainMaterial.white);

    murGauche.setPosition(c.largeur_mur_baginoire, 0);
    //enlever epaisseur carrelage ~ 1 cm
    World.scene.add(murGauche);

    var space = murGauche.sides.xMinus.get_space();

    var seche_serviette = new Box(0.54, 1.04, 0.04,  PlainMaterial.white);
    space.add(seche_serviette);
    seche_serviette.sides.zPlus.set_material(new SimpleImage("sdb/images/seche_serviette.png"));
    
    seche_serviette.setPosition(0.84, 0.59);

    seche_serviette.position.z=0.08;
    
    //hauteur baignoire=0.51
    carrelage(space, 2.23-0.8, c.hauteur_baignoire, 0.8, c.hauteur_mur-c.hauteur_baignoire, 0.15, 0.15, PlainMaterial.white, 0.002, PlainMaterial.white, 0.005, "mur_gauche" );
}